﻿# README #

Jest to studencki projekt małego robota autonomicznego, którego celem jest udział w kokurencji "Puck Collect". Robot wziął udział w zawodach Robomaticon 2016 w Warszawie, gdzie zajął pierwsze miejsce (patrz plik dyplom).
Robot jest wyposażony w:
 * czujniki IR do wykrywania przeszkód;
 * czujnik IR do wykrywania zebranych krążków;
 * 2 czujniki RGB po I2C - do ustalenia koloru bazy i zebranego krążka;
 * 2 silniki DC i enkodery kwadraturowe do nich;
 * moduł Bluetooth po UART do komunikacji z laptopem.
 
W osobnym [repozytorium](https://bitbucket.org/konar/upuck_wizualizacja/overview) znajduje się program do wizualizacji danych zebranych przez robota.