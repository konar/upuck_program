/**
 * @file color.h
 * @date 24.02.2016
 * @author Jacek Jankowski
 * @brief Color type definition.
 */

#ifndef APPLICATION_USER_COLOR_H_
#define APPLICATION_USER_COLOR_H_

enum color_t {
	white_color,
	red_color,
	blue_color
};

#endif /* APPLICATION_USER_COLOR_H_ */
