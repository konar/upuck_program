/**
 * @file coordinates.h
 * @date 25.02.2016
 * @author Jacek Jankowski
 * @brief Coordinate system for robot.
 */

#ifndef APPLICATION_USER_COORDINATES_H_
#define APPLICATION_USER_COORDINATES_H_

/**
 * @brief Coordinate system.
 */
struct coordinates_t {
	float x; /**< x coordinate in m. */
	float y; /**< y coorinate in m. */
	float theta; /**< Orientation angle in radians. */
};

#endif /* APPLICATION_USER_COORDINATES_H_ */
