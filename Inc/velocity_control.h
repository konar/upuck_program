/**
 * @file velocity_control.h
 *
 * @date 16.02.2015
 *
 * @author Jacek Jankowski
 */

#ifndef MOTORS_HH
#define MOTORS_HH

#include "tim.h"
#include <stdbool.h>

bool velocity_control_init(void);
void velocity_control_v_sp(float new_v);
void velocity_control_omega_sp(float new_omega);
bool velocity_control_update(float v, float omega);
int32_t velocity_control_return_left_pwm(void);
int32_t velocity_control_return_right_pwm(void);

#endif /* APPLICATION_USER_MLX90614_BAA_H_ */
