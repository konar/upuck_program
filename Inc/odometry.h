/**
 * @file odometry.h
 *
 * @date 16.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Odometry library.
 */

#ifndef APPLICATION_USER_ODOMETRY_H_
#define APPLICATION_USER_ODOMETRY_H_

#include <stdbool.h>
#include "coordinates.h"

bool odometry_init(struct coordinates_t coordinates);
bool odometry_update(float v, float omega);
struct coordinates_t odometry_return_coordinates(void);

#endif /* APPLICATION_USER_ODOMETRY_H_ */
