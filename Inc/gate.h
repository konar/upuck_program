/**
 * @file gate.h
 *
 * @date 11.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Library for  servo gate.
 */

#ifndef APPLICATION_USER_GATE_H_
#define APPLICATION_USER_GATE_H_

#include "tim.h"
#include <stdbool.h>

/**
 * @brief Type describing gate states.
 */
enum gate_order_t {
	gate_close,
	gate_open,
	gate_not_change
};

bool gate_init(TIM_HandleTypeDef * htim, uint32_t channel);
void gate_set(enum gate_order_t gate_order);

#endif /* APPLICATION_USER_GATE_H_ */
