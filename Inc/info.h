/**
 * @file info.h
 * @date 01.03.2016
 * @author Jacek Jankowski
 * @brief Informations from robot's body for brain.
 */

#ifndef APPLICATION_USER_INFO_H_
#define APPLICATION_USER_INFO_H_

#include "color.h"
#include "puck.h"
#include "coordinates.h"
#include <stdbool.h>

/**
 * @brief Informations from robot's body for brain.
 */
struct info_t {
	enum puck puck_info; /**< Information about founded puck. */
	enum color_t floor_color; /**< Actual floor color. */
	struct coordinates_t coordinates; /**< Actual robot's coordinates. */
	bool button_flag; /**< Button flag. */
	int32_t pwm_left; /**< PWM value for left motor. */
	int32_t pwm_right; /**< PWM value for right motor. */
};

#endif /* APPLICATION_USER_INFO_H_ */
