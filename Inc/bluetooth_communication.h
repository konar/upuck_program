/**
 * @file bluetooth_communication.h
 * @date 27.02.2016
 * @author Jacek Jankowski
 * @brief Module for Bluetooth communication between robot and user's computer.
 */

#ifndef APPLICATION_USER_BLUETOOTH_COMMUNICATION_H_
#define APPLICATION_USER_BLUETOOTH_COMMUNICATION_H_

#include "usart.h"
#include <stdint.h>
#include <stdbool.h>
#include "coordinates.h"

bool bluetooth_communication_init(UART_HandleTypeDef * huart);
bool bluetooth_communication_update(struct coordinates_t coordinates);

#endif /* APPLICATION_USER_BLUETOOTH_COMMUNICATION_H_ */
