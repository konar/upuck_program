/**
 * @file encoders.h
 *
 * @date 16.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Library for encoders.
 */

#ifndef APPLICATION_USER_ENCODERS_H_
#define APPLICATION_USER_ENCODERS_H_

#include "tim.h"
#include <stdbool.h>

bool encoders_init(TIM_HandleTypeDef * htim_left, TIM_HandleTypeDef * htim_right, float r, float l, float imp_to_rad);
bool encoders_update(void);
float encoders_return_v(void);
float encoders_return_omega(void);

#endif /* APPLICATION_USER_ENCODERS_H_ */
