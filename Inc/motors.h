/**
 * @file motors.h
 *
 * @date 16.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Library for motors.
 */

#ifndef APPLICATION_USER_MOTORS_H_
#define APPLICATION_USER_MOTORS_H_

#include "tim.h"
#include <stdbool.h>

bool motors_init(TIM_HandleTypeDef * htim_bridge, uint32_t bridge_left_channel,
		uint32_t bridge_right_channel, uint32_t pwm_limit);
void motors_pwm(int32_t left, int32_t right);

#endif /* APPLICATION_USER_MOTORS_H_ */
