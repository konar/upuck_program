/**
 * @file brain.h
 * @date 01.03.2016
 * @author Jacek Jankowski
 * @brief Behavioral brain module.
 */

#ifndef APPLICATION_USER_BRAIN_H_
#define APPLICATION_USER_BRAIN_H_

#include "color.h"
#include <stdbool.h>
#include <stdint.h>
#include "orders.h"
#include "info.h"

bool brain_init(enum color_t base_color);
bool brain_update(struct info_t info);
struct orders_t brain_return_orders(void);

#endif /* APPLICATION_USER_BRAIN_H_ */
