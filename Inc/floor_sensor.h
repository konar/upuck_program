/**
 * @file floor_sensor.h
 * @date 24.02.2016
 * @author Jacek Jankowski
 * @brief Module for floor color sensor.
 */

#ifndef APPLICATION_USER_FLOOR_SENSOR_H_
#define APPLICATION_USER_FLOOR_SENSOR_H_

#include "i2c.h"
#include <stdbool.h>
#include "color.h"

bool floor_sensor_init(I2C_HandleTypeDef * hi2c);
bool floor_sensor_update(void);
enum color_t floor_sensor_return_color(void);

#endif /* APPLICATION_USER_FLOOR_SENSOR_H_ */
