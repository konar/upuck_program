/**
 * @file puck_detector.h
 * @date 24.02.2016
 * @author Jacek Jankowski
 * @brief Module for puck detector.
 */

#ifndef APPLICATION_USER_PUCK_DETECTOR_H_
#define APPLICATION_USER_PUCK_DETECTOR_H_

#include <stdbool.h>
#include "i2c.h"
#include <stdint.h>
#include "puck.h"
#include "adc.h"
#include "rgb_sensor.h"

bool puck_detector_init(I2C_HandleTypeDef * hi2c, ADC_HandleTypeDef * hadc);
bool puck_detector_update(void);
enum puck puck_detector_return_puck(void);

#endif /* APPLICATION_USER_PUCK_DETECTOR_H_ */
