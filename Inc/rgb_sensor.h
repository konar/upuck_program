/**
 * @file rgb_sensor.h
 *
 * @date 10.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Library for RGB sensor tcs34725.
 */

#ifndef APPLICATION_USER_RGB_SENSOR_H_
#define APPLICATION_USER_RGB_SENSOR_H_

#include "i2c.h"
#include <stdbool.h>

bool color_sensor_init(I2C_HandleTypeDef * hi2c);
bool color_sensor_read(I2C_HandleTypeDef * hi2c, uint8_t color_data[8]);
uint16_t color_sensor_return_hue(uint8_t color_data[8]);
uint16_t color_sensor_return_clear(uint8_t color_data[8]);
uint16_t color_sensor_return_red(uint8_t color_data[8]);
uint16_t color_sensor_return_green(uint8_t color_data[8]);
uint16_t color_sensor_return_blue(uint8_t color_data[8]);
uint16_t color_sensor_return_saturation(uint8_t color_data[8]);

#endif /* APPLICATION_USER_RGB_SENSOR_H_ */
