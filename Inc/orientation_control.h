/**
 * @file orientation_control.c
 * @date 21.02.2016
 * @author Jacek Jankowski
 * @brief Real-time orientation control module.
 */

#ifndef APPLICATION_USER_ORIENTATION_CONTROL_H_
#define APPLICATION_USER_ORIENTATION_CONTROL_H_

void orientation_control_init(float k);
void orientation_control_set_goal(float theta);
void orientation_control_update(float theta);
float orientation_control_return_omega(void);

#endif /* APPLICATION_USER_ORIENTATION_CONTROL_H_ */
