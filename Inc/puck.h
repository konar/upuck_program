/**
 * @file puck.h
 * @date 24.02.2016
 * @author Jacek Jankowski
 * @brief Type to describe puck.
 */

#ifndef APPLICATION_USER_PUCK_H_
#define APPLICATION_USER_PUCK_H_

/**
 * @brief Type describing puck.
 */
enum puck {no_puck, red_puck, blue_puck};

#endif /* APPLICATION_USER_PUCK_H_ */
