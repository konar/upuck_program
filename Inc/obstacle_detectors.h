/**
 * @file obstacle_detectors.h
 * @date 24.02.2016
 * @author Jacek Jankowski
 * @brief Module for obstacle detectors.
 */

#ifndef APPLICATION_USER_OBSTACLE_DETECTORS_H_
#define APPLICATION_USER_OBSTACLE_DETECTORS_H_

#include <stdbool.h>

/**
 * @brief Type describing obstacle detector.
 */
enum obstacle_detector {
	right_back_obstacle_detector,
	right_side_obstacle_detector,
	right_front_obstacle_detector,
	left_front_obstacle_detector,
	left_side_obstacle_detector,
	left_back_obstacle_detector
};

bool obstacle_detectors_init(void);
bool obstacle_detectors_update(void);
bool obstacle_detectors_check(enum obstacle_detector detector);

#endif /* APPLICATION_USER_OBSTACLE_DETECTORS_H_ */
