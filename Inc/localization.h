/**
 * @file localization.h
 * @date 20.02.2016
 * @author Jacek Jankowski
 * @brief Real-time localization module.
 */

#ifndef APPLICATION_USER_LOCALIZATION_H_
#define APPLICATION_USER_LOCALIZATION_H_

#include <stdbool.h>
#include <stdint.h>
#include "color.h"
#include "coordinates.h"

bool localization_init(enum color_t color_0);
bool localization_update(struct coordinates_t coordinates_odo, enum color_t floor_color);
struct coordinates_t localization_return_coordinates(void);

#endif /* APPLICATION_USER_LOCALIZATION_H_ */
