/**
 * @file orders.h
 * @date 01.03.2016
 * @author Jacek Jankowski
 * @brief Orders from brain to robot's body.
 */

#ifndef APPLICATION_USER_ORDERS_H_
#define APPLICATION_USER_ORDERS_H_

#include <stdbool.h>
#include "gate.h"

/**
 * @brief Orders from brain to robot's body.
 */
struct orders_t {
	float v; /**< Robot forward velocity in m/s. */
	float omega; /**< Robot angular velocity in rad/s. */
	enum gate_order_t gate; /**< If gate should be open. */
};

#endif /* APPLICATION_USER_ORDERS_H_ */
