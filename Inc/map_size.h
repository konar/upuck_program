/**
 * @file map_size.h
 * @date 02.03.2016
 * @author Jacek Jankowski
 * @brief Sizes of map and bases.
 */

#ifndef APPLICATION_USER_MAP_SIZE_H_
#define APPLICATION_USER_MAP_SIZE_H_

/** Pi value. */
#define M_PI 3.14159265358979323846

/** @brief Ring X size in m. */
#define RING_X_SIZE 0.7

/** @brief Ring Y size in m. */
#define RING_Y_SIZE 1.0

/** @brief Base X size in m. */
#define BASE_X_SIZE 0.21

/** @brief Base Y size in m. */
#define BASE_Y_SIZE 0.3

#endif /* APPLICATION_USER_MAP_SIZE_H_ */
