/**
  ******************************************************************************
  * File Name          : mxconstants.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define BUTTON_Pin GPIO_PIN_1
#define BUTTON_GPIO_Port GPIOD
#define CNY70_Pin GPIO_PIN_0
#define CNY70_GPIO_Port GPIOA
#define SERVO_Pin GPIO_PIN_3
#define SERVO_GPIO_Port GPIOA
#define RIGHT_BACK_Pin GPIO_PIN_4
#define RIGHT_BACK_GPIO_Port GPIOA
#define RIGHT_SIDE_Pin GPIO_PIN_5
#define RIGHT_SIDE_GPIO_Port GPIOA
#define RIGHT_FRONT_Pin GPIO_PIN_6
#define RIGHT_FRONT_GPIO_Port GPIOA
#define LEFT_FRONT_Pin GPIO_PIN_7
#define LEFT_FRONT_GPIO_Port GPIOA
#define LEFT_SIDE_Pin GPIO_PIN_4
#define LEFT_SIDE_GPIO_Port GPIOC
#define LEFT_BACK_Pin GPIO_PIN_5
#define LEFT_BACK_GPIO_Port GPIOC
#define SCL2_Pin GPIO_PIN_10
#define SCL2_GPIO_Port GPIOB
#define SDA2_Pin GPIO_PIN_11
#define SDA2_GPIO_Port GPIOB
#define TX_Pin GPIO_PIN_9
#define TX_GPIO_Port GPIOA
#define RX_Pin GPIO_PIN_10
#define RX_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define PWMR_Pin GPIO_PIN_15
#define PWMR_GPIO_Port GPIOA
#define RIN2_Pin GPIO_PIN_10
#define RIN2_GPIO_Port GPIOC
#define RIN1_Pin GPIO_PIN_11
#define RIN1_GPIO_Port GPIOC
#define LIN1_Pin GPIO_PIN_12
#define LIN1_GPIO_Port GPIOC
#define LIN2_Pin GPIO_PIN_2
#define LIN2_GPIO_Port GPIOD
#define PWML_Pin GPIO_PIN_3
#define PWML_GPIO_Port GPIOB
#define LENA_Pin GPIO_PIN_4
#define LENA_GPIO_Port GPIOB
#define LENB_Pin GPIO_PIN_5
#define LENB_GPIO_Port GPIOB
#define RENB_Pin GPIO_PIN_6
#define RENB_GPIO_Port GPIOB
#define RENA_Pin GPIO_PIN_7
#define RENA_GPIO_Port GPIOB
#define SCL1_Pin GPIO_PIN_8
#define SCL1_GPIO_Port GPIOB
#define SDA1_Pin GPIO_PIN_9
#define SDA1_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
