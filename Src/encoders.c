/**
 * @file encoders.c
 *
 * @date 16.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Library for encoders.
 */

#include "encoders.h"

/** Time interval in ms used to compute angular velocity in function encoders_update() */
#define DELTA_T_MS 10

/**
 * @brief Pointer to timer handle used by left encoder.
 */
static TIM_HandleTypeDef * _htim_left;

/**
 * @brief Pointer to timer handle used by right encoder.
 */
static TIM_HandleTypeDef * _htim_right;

/**
 * @brief Robot angular velocity in rad/s.
 */
volatile static float _omega;

/**
 * @brief Robot forward velocity in m/s.
 */
volatile static float _v;

/**
 * @brief Wheel radius in m.
 */
volatile static float _r;

/**
 * @brief Distance between wheels in m.
 */
volatile static float _l;

/**
 * @brief Encoder impulse to radian ratio.
 */
volatile static float _imp_to_rad;

float _compute_phi_dot(int32_t cnt_table[]);

/**
 * @brief Return robot forward velocity in m/s.
 *
 * @return Robot forward velocity in m/s.
 */
float encoders_return_v(void){
	return _v;
}

/**
 * @brief Return robot angular velocity in rad/s.
 *
 * @return Robot angular velocity in rad/s.
 */
float encoders_return_omega(void){
	return _omega;
}

/**
 * @brief Initialize encoders to work.
 *
 * @param htim_en_left - Pointer to timer handle used by left encoder.
 * @param htim_en_right - Pointer to timer handle used by right encoder.
 * @param r - Wheel radius in m.
 * @param l - Distance between wheels in m.
 * @param imp_to_rad - Encoder impulse to radian ratio.
 *
 * @return If everything was successfully initialized.
 */
bool encoders_init(TIM_HandleTypeDef * htim_left, TIM_HandleTypeDef * htim_right, float r, float l, float imp_to_rad){
	bool retval = true;

	_htim_left = htim_left;
	_htim_right = htim_right;
	_r = r;
	_l = l;
	_imp_to_rad = imp_to_rad;

	retval = HAL_TIM_Encoder_Start(_htim_left, TIM_CHANNEL_ALL) == HAL_OK ? retval : false;
	retval = HAL_TIM_Encoder_Start(_htim_right, TIM_CHANNEL_ALL) == HAL_OK ? retval : false;

	return retval;
}

/**
 * @brief Compute wheel angular velocity in rad/s.
 *
 * @param cnt_table - Table with #DELTA_T_MS consecutive counter values.
 * The first element should be the newest value.
 *
 * @return Wheel angular velocity in rad/s.
 */
float _compute_phi_dot(int32_t cnt_table[]){
	int32_t phi_dot = cnt_table[0]-cnt_table[DELTA_T_MS-1];

	if(phi_dot > 60000){
		phi_dot = -((0xffff - cnt_table[0]) + cnt_table[DELTA_T_MS-1]);
	}
	else if(phi_dot < -60000){
		phi_dot = (0xffff - cnt_table[DELTA_T_MS-1]) + cnt_table[0];
	}

	return (phi_dot * (1000/DELTA_T_MS)) / _imp_to_rad;
}

/**
 * @brief Real time velocity measure.
 *
 * Real time forward and angular velocity measure.
 * This function should be invoked every 1 ms.
 *
 * @return If everything was successfully updated.
 */
bool encoders_update(void){
	static int32_t left_cnt_t[DELTA_T_MS];
	static int32_t right_cnt_t[DELTA_T_MS];

	for(unsigned i = 0; i < DELTA_T_MS-1; i++){
		left_cnt_t[DELTA_T_MS-1-i] = left_cnt_t[DELTA_T_MS-2-i];
		right_cnt_t[DELTA_T_MS-1-i] = right_cnt_t[DELTA_T_MS-2-i];
	}
	left_cnt_t[0] = _htim_left->Instance->CNT;
	right_cnt_t[0] = _htim_right->Instance->CNT;

	float phi_dot_left = _compute_phi_dot(left_cnt_t);
	float phi_dot_right = _compute_phi_dot(right_cnt_t);

	_v = ((phi_dot_left+phi_dot_right)*_r)/2;
	_omega = ((phi_dot_right - phi_dot_left)*_r)/_l;

	return true;
}
