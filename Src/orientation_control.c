/**
 * @file orientation_control.c
 * @date 21.02.2016
 * @author Jacek Jankowski
 * @brief Real-time orientation control module.
 */

#include "orientation_control.h"

/** Pi value. */
#define M_PI 3.14159265358979323846

/**
 * @brief Proportional gain of orientation controller.
 */
volatile static float _k;

/**
 * @brief Orientation goal in radians.
 */
volatile static float _goal;

/**
 * @brief Angluar velocity in rad/s needed to achieve/maintain given orientation.
 */
volatile static float _omega;

/**
 * @brief Return angular velocity.
 *
 * Return angluar velocity in rad/s
 * needed to achieve/maintain given orientation.
 */
float orientation_control_return_omega(void){
	return _omega;
}

/**
 * @brief Initialize orientation control module.
 * @param k - Proportional gain of controller.
 */
void orientation_control_init(float k){
	_k = k;
}

/**
 * @brief Set orientation goal.
 * @param theta - new orientation goal in radians.
 */
void orientation_control_set_goal(float goal){
	_goal = goal;
}

/**
 * @brief Real time orientation control.
 *
 * This function should be invoked every 1 ms.
 *
 * @param theta - actual orientation angle in radians.
 */
void orientation_control_update(float theta){
	float tmp_theta = theta - M_PI;
	float tmp_goal = _goal - M_PI;
	_omega = (tmp_goal-tmp_theta)*_k;
}
