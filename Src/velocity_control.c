/**
 * @file velocity_control.c
 *
 * @date 16.02.2015
 *
 * @author Jacek Jankowski
 */

#include "velocity_control.h"

/**
 * @brief Forward velocity setpoint in m/s.
 */
volatile static float _v_sp;

/**
 * Angular velocity setpoint in rad/s.
 */
volatile static float _omega_sp;

/**
 * @brief PWM value for left value.
 */
volatile static int32_t _pwm_left;

/**
 * @brief PWM value for right value.
 */
volatile static int32_t _pwm_right;

int32_t _omega_controller(float error);
int32_t _forward_controller(float error);

/**
 * @brief Initialize velocity control.
 *
 * @return If everything was successfully initialized.
 */
bool velocity_control_init(void){
	return true;
}

/**
 * @brief Return PWM value for left wheel.
 *
 * @return PWM value for left wheel.
 */
int32_t velocity_control_return_left_pwm(void){
	return _pwm_left;
}

/**
 * @brief Return PWM value for right wheel.
 *
 * @return PWM value for right wheel.
 */
int32_t velocity_control_return_right_pwm(void){
	return _pwm_right;
}

/**
 * @brief Change forward velocity setpoint.
 *
 * @param new_v - New forward velocity setpoint in m/s.
 */
void velocity_control_v_sp(float new_v){
	_v_sp = new_v;
}

/**
 * @brief Change angular velocity setpoint.
 *
 * @param new_omega - New angular velocity setpoint in rad/s.
 */
void velocity_control_omega_sp(float new_omega){
	_omega_sp = new_omega;
}

/**
 * @brief Forward velocity controller.
 *
 * @param error - Forward velocity error in m/s.
 *
 * @return Controller output.
 */
int32_t _forward_controller(float error){
	static float I = 0;

	float dead_zone = 0;
	float step = 0.1;

	if(error > 0.2 || error < -0.2){
		step *= 2;
	}

	if(error > dead_zone){
		I = I + step;
	}
	else if(error < -dead_zone){
		I = I - step;
	}

	float limit = 40;
	I = (I>limit) ? limit : I;
	limit = -limit;
	I = (I<limit) ? limit : I;

	return I;
}

/**
 * @brief Angular velocity controller.
 *
 * @param error - Angular velocity error in rad/s.
 *
 * @return Controller output.
 */
int32_t _omega_controller(float error){
	static float I = 0;

	float dead_zone = 0;
	float step = 0.1;
	if(error > dead_zone){
		I = I + step;
	}
	else if(error < -dead_zone){
		I = I - step;
	}

	float limit = 40;
	I = (I>limit) ? limit : I;
	limit = -limit;
	I = (I<limit) ? limit : I;

	return I;
}

/**
 * @brief Real time velocity control.
 *
 * Function is responsible for real time velocity control.
 * It should be invoked every 1 ms.
 *
 * @param v - Actual forward velocity in m/s.
 * @param omega - Actual angular velocity in rad/s.
 *
 * @return If everything was successfully updated.
 */
bool velocity_control_update(float v, float omega){
	int32_t pwm_forward = _forward_controller(_v_sp-v);
	int32_t pwm_angular = _omega_controller(_omega_sp-omega);

	_pwm_right = pwm_forward + pwm_angular;
	_pwm_left = pwm_forward - pwm_angular;

	return true;
}
