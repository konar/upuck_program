/**
 * @file brain.c
 * @date 01.03.2016
 * @author Jacek Jankowski
 * @brief Behavioral brain module.
 */

#include "brain.h"
#include "map_size.h"
#include "math.h"

/** Forward velocity in m/s for seek mode. */
#define V_SEEK 0.6

/** Angular velocity in m/s for seek mode. */
#define OMEGA_SEEK 4

/** Forward velocity in m/s for deliver mode. */
#define V_DELIVER 0.5

/** Angular velocity in m/s for deliver mode. */
#define OMEGA_DELIVER 4

/** Velocity in m/s for returning puck to base. */
#define V_RELEASE 0.5

/** Collision detection threshold for PWM value. */
#define COLLSION_DETECTION_TRESHOLD 33

#define COLLSION_DETECTION_TRESHOLD_DELIVER_BIAS 4

#define DEAD_CLOCK_TRESHOLD 7000

static volatile int32_t dead_clock = 0;

/**
 * @brief Color of the start base.
 */
static volatile enum color_t _brain_start_color;

/**
 * @brief Brain counter.
 *
 * If it is equal 0, it means that the brain need to chose next behavior.
 */
static volatile uint32_t _brain_cnt;

/**
 * @brief Set of robot behaviors.
 */
enum behavior_t {
	wait,
	seek_catch,
	seek_forward,
	seek_backward,
	seek_left,
	deliver_forward,
	deliver_backward,
	deliver_left,
	release_backward
};

/** Current behavior. */
static volatile enum behavior_t _brain_current_behavior;

/**
 * @brief Initialize brain module.
 *
 * @param base_color - Color of the start base.
 *
 * @return If everything was successfully initialized.
 */
bool brain_init(enum color_t base_color){
	bool retval = true;

	_brain_current_behavior = wait;

	_brain_start_color = base_color;
	retval = base_color != white_color ? retval : false;

	return retval;
}

/**
 * @brief Compute angle error if you want to get to point (x_g, y_g).
 *
 * @param x_g - Coordinate X for goal point.
 * @param y_g - Coordinate Y for goal point.
 * @param info - Actual information from robot's body.
 *
 * @return Angle error if you want to get to point (x_g, y_g).
 */
float _brain_compute_angle_error(float x_g, float y_g, struct info_t info){
	float theta_g = atan2f(y_g-info.coordinates.y, x_g-info.coordinates.x);
	theta_g = theta_g>=0 ? theta_g : (2*M_PI+theta_g);
	float error = theta_g - info.coordinates.theta;
	error = error > M_PI ? theta_g - info.coordinates.theta - 2*M_PI : error;
	error = error < -M_PI ? theta_g + 2*M_PI - info.coordinates.theta : error;
	return error;
}

/**
 * @brief Collision detection.
 *
 * @param pwm_left - PWM value for left motor.
 * @param pwm_right - PWM value for right motor.
 *
 * @return If collision was detected.
 */
bool _brain_collsion_detection(struct info_t info){
	int32_t bias = 0;
	if(_brain_current_behavior == deliver_forward){
		bias = COLLSION_DETECTION_TRESHOLD_DELIVER_BIAS;
	}
	if(
			info.pwm_left >= (COLLSION_DETECTION_TRESHOLD + bias) ||
			info.pwm_right >= (COLLSION_DETECTION_TRESHOLD + bias) ||
			info.pwm_left <= -(COLLSION_DETECTION_TRESHOLD + bias) ||
			info.pwm_right <= -(COLLSION_DETECTION_TRESHOLD + bias) ||
			dead_clock > DEAD_CLOCK_TRESHOLD
	){
		dead_clock = 0;
		return true;
	}
	else{
		return false;
	}
}

/**
 * @brief Select behavior based on available information.
 *
 * @param info - Informations from robot's body for brain.
 *
 */
void _switch_behavior_brain(struct info_t info){

	if // start button pressed
	(info.button_flag) {
		/////////////////////////////////////////////////
		////////////////////// RETURN ///////////////////
		/////////////////////////////////////////////////
		if // we just released puck
		(_brain_current_behavior == release_backward){
			_brain_current_behavior = seek_left;
			_brain_cnt = 500;

		}
		else if // robot has good puck
		(
				_brain_current_behavior == seek_catch ||
				_brain_current_behavior == deliver_forward ||
				_brain_current_behavior == deliver_backward ||
				_brain_current_behavior == deliver_left
		){
			if // we are in good base!
			(_brain_start_color == info.floor_color)
			{
				_brain_current_behavior = release_backward;
				_brain_cnt = 500;
			}
			else if //we are recovering from collision
			(_brain_current_behavior == deliver_backward){
				_brain_current_behavior = deliver_left;
				_brain_cnt = 350;
			}
			else if //collision detected
			(_brain_current_behavior == deliver_forward && _brain_collsion_detection(info)){
				_brain_current_behavior = deliver_backward;
				_brain_cnt = 350;
			}
			else //going forward
			{
				if(_brain_current_behavior != deliver_forward){
					dead_clock = 0;
				}
				_brain_current_behavior = deliver_forward;
				_brain_cnt = 0;
				dead_clock++;
			}
		}
		/////////////////////////////////////////////////
		////////////////// SEEK SECTION /////////////////
		/////////////////////////////////////////////////
		else // so far robot hasn't found a good puck
		{
			if//we found a good puck!
			(
					(info.puck_info == red_puck && _brain_start_color == red_color) ||
					(info.puck_info == blue_puck && _brain_start_color == blue_color)
			){
				_brain_current_behavior = seek_catch;
				_brain_cnt = 300;
			}
			else if // right now we found a bad puck or collided with obstacle (wall or other robot)
			(_brain_current_behavior == seek_forward &&
					(
							(info.puck_info == red_puck && _brain_start_color == blue_color) ||
							(info.puck_info == blue_puck && _brain_start_color == red_color) ||
							_brain_collsion_detection(info)
					)
			){
				_brain_current_behavior = seek_backward;
				_brain_cnt = 500;
			}
			else if // we need to turn left after releasing puck or collision with wall
			(_brain_current_behavior == seek_backward){
				_brain_current_behavior = seek_left;
				_brain_cnt = 500;
			}
			else if // the last step of avoiding bad puck or obstacle
			(_brain_current_behavior == seek_left)
			{
				_brain_current_behavior = seek_forward;
				_brain_cnt = 300;
			}
			else
			{// no collision
				if(_brain_current_behavior != seek_forward){
					dead_clock = 0;
				}
				_brain_current_behavior = seek_forward;
				_brain_cnt = 0;
				dead_clock++;
			}
		}
	}
	else
	{// waiting for start button
		_brain_current_behavior = wait;
		_brain_cnt = 0;
	}
}

/**
 * @brief Update brain module with new informations.
 *
 * This function should be invoked every 1 ms.
 *
 * @param info - Informations from robot's body for brain.
 *
 * @return If everything was successfully updated.
 */
bool brain_update(struct info_t info){
	if(_brain_cnt){
		_brain_cnt--;
	}

	if(_brain_cnt == 0){
		_switch_behavior_brain(info);
	}

	return true;
}

/**
 * @brief Return brain orders for robot's body orders.
 *
 * @return Brain orders for robot's body orders.
 */
struct orders_t brain_return_orders(void){
	struct orders_t retval = {gate_not_change, 0, 0};

	switch(_brain_current_behavior){

	case wait:
		retval.gate = gate_not_change;
		retval.omega = 0;
		retval.v = 0;
		break;

	case seek_catch:
		retval.gate = gate_close;
		retval.omega = 0;
		retval.v = V_SEEK;
		break;
	case seek_forward:
		retval.gate = gate_open;
		retval.omega = 0;
		retval.v = V_SEEK;
		break;
	case seek_backward:
		retval.gate = gate_open;
		retval.omega = 0;
		retval.v = -V_SEEK;
		break;
	case seek_left:
		retval.gate = gate_open;
		retval.omega = OMEGA_SEEK;
		retval.v = 0;
		break;

	case deliver_forward:
		retval.gate = gate_close;
		retval.omega = -OMEGA_DELIVER/7;
		retval.v = V_DELIVER;
		break;
	case deliver_backward:
		retval.gate = gate_close;
		retval.omega = 0;
		retval.v = -V_DELIVER;
		break;
	case deliver_left:
		retval.gate = gate_close;
		retval.omega = OMEGA_DELIVER;
		retval.v = 0;
		break;

	case release_backward:
		retval.gate = gate_open;
		retval.omega = 0;
		retval.v = -V_RELEASE;
		break;
	}

	return retval;
}
