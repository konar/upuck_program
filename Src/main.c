/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include "rgb_sensor.h"
#include "gate.h"
#include "velocity_control.h"
#include "encoders.h"
#include "motors.h"
#include "mxconstants.h"
#include "odometry.h"
#include "localization.h"
#include "orientation_control.h"
#include "floor_sensor.h"
#include "puck_detector.h"
#include "obstacle_detectors.h"
#include "coordinates.h"
#include "bluetooth_communication.h"
#include "brain.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
volatile enum puck puck_info;
volatile enum color_t floor_color;

volatile float v;
volatile float omega;

volatile int32_t pwm_left;
volatile int32_t pwm_right;

struct coordinates_t coordinates;

volatile uint8_t wall_detectors[6];

volatile int init_success = true;
volatile int update_success = true;

volatile uint8_t flag_1ms;
volatile uint8_t flag_500ms;
volatile uint8_t flag_button;

volatile struct orders_t orders;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_SYSTICK_Callback(void){
	if(HAL_GetTick()%1 == 0){
		flag_1ms = 1;
	}
	if(HAL_GetTick()%500 == 0){
		flag_500ms = 1;
	}
}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
/** Wheel radius. */
#define WHEEL_RADIUS 0.030

/** Distance between wheels. */ //0.1
#define L 0.1

#define M_PI 3.14159265358979323846

/** Impulse to radian ratio. */
#define IMPULSE_TO_RADIAN_RATIO 150/M_PI
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM15_Init();
  MX_USART1_UART_Init();

  /* USER CODE BEGIN 2 */
	init_success = true;

	init_success = puck_detector_init(&hi2c1, &hadc1) ? init_success : false;
	init_success = floor_sensor_init(&hi2c2) ? init_success : false;
	HAL_Delay(10);
	for(int i = 0; i < 3; i++){
		update_success = puck_detector_update() ? update_success : false;
		HAL_Delay(1);
		update_success = floor_sensor_update() ? update_success : false;
		HAL_Delay(1);
	}
	floor_color = floor_sensor_return_color();
	init_success = (floor_color != white_color) ? init_success : false;

	init_success = obstacle_detectors_init() ? init_success : false;

	init_success = encoders_init(&htim3, &htim4, WHEEL_RADIUS, L, IMPULSE_TO_RADIAN_RATIO) ? init_success : false;
	init_success = odometry_init(coordinates) ? init_success : false;

	init_success = localization_init(floor_color) ? init_success : false;
	coordinates = localization_return_coordinates();

	init_success = velocity_control_init() ? init_success : false;
	init_success = motors_init(&htim2, TIM_CHANNEL_2, TIM_CHANNEL_1, 40) ? init_success : false;

	init_success = bluetooth_communication_init(&huart1) ? init_success : false;

	init_success = brain_init(floor_color) ? init_success : false;

	while(!init_success || !init_success){
		;
	}

	init_success = gate_init(&htim15, TIM_CHANNEL_2) ? init_success : false;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while(!init_success || !init_success){
		;
	}

	while (1)
	{
		if(flag_1ms){
			flag_1ms = 0;

			if(HAL_GPIO_ReadPin(BUTTON_GPIO_Port,BUTTON_Pin) == GPIO_PIN_RESET){
				HAL_Delay(500);
				flag_button = 1;
			}

			update_success = puck_detector_update() ? update_success : false;
			puck_info = puck_detector_return_puck();

			update_success = floor_sensor_update() ? update_success : false;
			floor_color = floor_sensor_return_color();

			update_success = obstacle_detectors_update() ? update_success : false;
			for(uint8_t i = right_back_obstacle_detector; i <= left_back_obstacle_detector; i++){
				wall_detectors[i] = obstacle_detectors_check(i);
			}

			update_success = encoders_update() ? update_success : false;
			v = encoders_return_v();
			omega = encoders_return_omega();

			update_success = velocity_control_update(v, omega) ? update_success : false;
			pwm_left = velocity_control_return_left_pwm();
			pwm_right = velocity_control_return_right_pwm();

			motors_pwm(pwm_left, pwm_right);

			odometry_init(coordinates);
			update_success = odometry_update(v, omega) ? update_success : false;
			update_success = localization_update(odometry_return_coordinates(), floor_color) ? update_success : false;
			coordinates = localization_return_coordinates();

			struct info_t info = {puck_info,floor_color,coordinates, flag_button, pwm_left, pwm_right};
			update_success = brain_update(info) ? update_success : false;
			orders = brain_return_orders();
			velocity_control_v_sp(orders.v);
			velocity_control_omega_sp(orders.omega);
			gate_set(orders.gate);

		}

		if(flag_500ms){
			flag_500ms = 0;
			update_success = bluetooth_communication_update(coordinates) ? update_success : false;
		}
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

	}
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0);

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV2;
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
