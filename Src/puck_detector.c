/**
 * @file puck_detector.c
 * @date 24.02.2016
 * @author Jacek Jankowski
 * @brief Module for puck detector.
 */

#include "puck_detector.h"

/**
 * @brief Threshold for ADC value from CNY70 sensor.
 */
#define CNY70_THRESHOLD 3850

/**
 * @brief Pointer to I2C handle used by puck detector.
 */
static I2C_HandleTypeDef * _hi2c;

/**
 * @brief Pointer to ADC handle used by puck detector.
 */
static ADC_HandleTypeDef * _hadc;

/**
 * @brief Raw data from RGB sensor.
 */
static uint8_t _data[8];

/**
 * @brief Information about puck presence and color.
 */
static enum puck _puck;

/**
 * @brief Initialize module detecting puck.
 *
 * @param hi2c - Pointer to I2C handle used by puck detector.
 * @param hadc - Pointer to ADC handle used by puck detector.
 *
 * @return If everything was successfully initialized.
 */
bool puck_detector_init(I2C_HandleTypeDef * hi2c, ADC_HandleTypeDef * hadc){
	bool retval = true;
	_hi2c = hi2c;
	_hadc = hadc;
	if(HAL_ADC_Start(_hadc) != HAL_OK){
		retval = false;
	}
	if(color_sensor_init(_hi2c) != true){
		retval = false;
	}
	return retval;
}

/**
 * @brief Update information from sensors.
 *
 * This function should be invoked every 1 ms.
 *
 * @return If everything was successfully updated.
 */
bool puck_detector_update(void){
	bool retval = true;

	uint16_t cny70_adc = HAL_ADC_GetValue(_hadc);
	uint16_t hue = color_sensor_return_hue(_data);

	if(cny70_adc > CNY70_THRESHOLD){
		_puck = no_puck;
	}
	else if(hue >= 120 && hue <= 300){ // blue
		_puck = blue_puck;
	}
	else{ // red
		_puck = red_puck;
	}

	// this order is OK becouse color_sensor_read uses interrupts to deal with I2C
	if(HAL_ADC_Start(_hadc) != HAL_OK){
		retval = false;
	}
	if(color_sensor_read(_hi2c, _data) != true){
		retval = false;
	}
	return retval;
}

/**
 * @brief Return information about puck presence and color.
 *
 * @brief Information about puck presence and color.
 */
enum puck puck_detector_return_puck(void){
	return _puck;
}
