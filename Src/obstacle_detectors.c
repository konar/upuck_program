/**
 * @file obstacle_detectors.c
 * @date 24.02.2016
 * @author Jacek Jankowski
 * @brief Module for obstacle detectors.
 */

#include "obstacle_detectors.h"
#include "mxconstants.h"
#include "gpio.h"

/**
 * @brief Initialize peripherals used by obstacle detectors.
 *
 * @return If everything was successfully initialized.
 */
bool obstacle_detectors_init(void){
	bool retval = true;

	return retval;
}

/**
 * @brief Update information from obstacle detectors.
 *
 * This function should be invoked every 1ms.
 *
 * @return If everything was successfully updated.
 */
bool obstacle_detectors_update(void){
	bool retval = true;

	return retval;
}

/**
 * @brief Check obstacle presence.
 *
 * @param detector - Detector to be used.
 *
 * @return If obstacle detected.
 */
bool obstacle_detectors_check(enum obstacle_detector detector){
	bool retval = false;

	switch(detector){
	case right_back_obstacle_detector: retval = !HAL_GPIO_ReadPin(RIGHT_BACK_GPIO_Port, RIGHT_BACK_Pin); break;
	case right_side_obstacle_detector: retval = !HAL_GPIO_ReadPin(RIGHT_SIDE_GPIO_Port, RIGHT_SIDE_Pin); break;
	case right_front_obstacle_detector: retval = !HAL_GPIO_ReadPin(RIGHT_FRONT_GPIO_Port, RIGHT_FRONT_Pin); break;
	case left_front_obstacle_detector: retval = !HAL_GPIO_ReadPin(LEFT_FRONT_GPIO_Port, LEFT_FRONT_Pin); break;
	case left_side_obstacle_detector: retval = !HAL_GPIO_ReadPin(LEFT_SIDE_GPIO_Port, LEFT_SIDE_Pin); break;
	case left_back_obstacle_detector: retval = !HAL_GPIO_ReadPin(LEFT_BACK_GPIO_Port, LEFT_BACK_Pin); break;
	}

	return retval;
}
