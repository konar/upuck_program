/**
 * @file bluetooth_communication.h
 * @date 27.02.2016
 * @author Jacek Jankowski
 * @brief Module for Bluetooth communication between robot and user's computer.
 */

#include "bluetooth_communication.h"

/** Length of data frame. */
#define FRAME_LENGTH 13

/**
 * @brief Place for data sended to user's computer.
 */
uint8_t _bluetooth_communication_data[FRAME_LENGTH];

/**
 * @brief Pointer to UART handler used by Bluetooth.
 */
UART_HandleTypeDef * _huart;

/**
 * @brief Initialize Bluetooth communication.
 *
 * @param huart - Pointer to UART handler used by Bluetooth.
 *
 * @return If everything was initialized successfully.
 */
bool bluetooth_communication_init(UART_HandleTypeDef * huart){
	_huart = huart;
	_bluetooth_communication_data[0] = '!';
	return true;
}

/**
 * @brief Update information sending to user's comupter.
 *
 * @param coordinates - Robot coordinates.
 *
 * @return If everything was successfully updated.
 */
bool bluetooth_communication_update(struct coordinates_t coordinates){
	float * ptr;

	ptr = (float*)(_bluetooth_communication_data+1);
	*ptr = coordinates.x;

	ptr = ptr+1;
	*ptr = coordinates.y;

	ptr = ptr+1;
	*ptr = coordinates.theta;

	if(HAL_UART_Transmit_DMA(_huart, _bluetooth_communication_data, FRAME_LENGTH) == HAL_OK){
		return true;
	}
	else{
		return false;
	}
}
