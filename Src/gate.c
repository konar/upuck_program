/**
 * @file gate.c
 *
 * @date 11.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Library for  servo gate.
 */

#ifndef APPLICATION_USER_GATE_C_
#define APPLICATION_USER_GATE_C_

#include "gate.h"

/**
 * @brief Pointer to timer handle used by servo.
 */
static TIM_HandleTypeDef * _htim;

/**
 * @brief Pointer to timer channel used by servo.
 */
volatile static uint32_t * _channel;

/**
 * @brief Initialize servo gate.
 *
 * @param htim - Pointer to timer handle used by servo.
 * @param channel - Timer channel used by servo.
 *
 * @brief If everything was successfully initialized.
 */
bool gate_init(TIM_HandleTypeDef * htim, uint32_t channel){
	bool retval = true;

	_htim = htim;

	switch(channel){
	case TIM_CHANNEL_1: _channel = &(_htim->Instance->CCR1); break;
	case TIM_CHANNEL_2: _channel = &(_htim->Instance->CCR2); break;
	case TIM_CHANNEL_3: _channel = &(_htim->Instance->CCR3); break;
	default: _channel = &(_htim->Instance->CCR4); break;
	}

	gate_set(gate_open);
	retval = HAL_TIM_Base_Start(_htim) == HAL_OK ? retval : false;
	retval = HAL_TIM_PWM_Start(_htim, channel) == HAL_OK ? retval : false;

	return retval;
}

/**
 * @brief Change gate state.
 *
 * @param gate_order - New gate's state.
 */
void gate_set(enum gate_order_t gate_order){
	if(gate_order == gate_close){
		*_channel = 1;
	}
	else if(gate_order == gate_open){
		*_channel = 2;
	}
	else{
		;
	}
}

#endif /* APPLICATION_USER_GATE_C_ */
