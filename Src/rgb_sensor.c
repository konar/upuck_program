/**
 * @file rgb_sensor.c
 *
 * @date 10.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Library for RGB sensor tcs34725.
 */

#include "rgb_sensor.h"

#define DeviceAddress 0x29
#define RepeatedByteFlag 0x00
#define AutoIncrementFlag 0x20
#define CommandFlag 0x80
#define EnableAddress 0x00
// integration time
#define ATimeAddress 0x01
#define ControlAddress 0x0f
#define CDataLAddress 0x14
#define RDataLAddress 0x16
#define GDataLAddress 0x18
#define BDataLAddress 0x1a

/**
 * @brief Initialize color sensor in polling mode.
 *
 * @param hi2c - Pointer to I2C handler.
 *
 * @return If success.
 */
bool color_sensor_init(I2C_HandleTypeDef * hi2c){
	// integrity_time = (256 - register_value) x 2.4ms
	uint8_t ATimeRegister = 253; // 2.4ms
	if (HAL_I2C_Mem_Write(hi2c, DeviceAddress << 1, CommandFlag | RepeatedByteFlag | ATimeAddress, 1, &ATimeRegister, 1, 10) != HAL_OK) {
		return false;
	}

	uint8_t ControlRegister = 0x00; // 1x gain for RGBC
	if (HAL_I2C_Mem_Write(hi2c, DeviceAddress << 1, CommandFlag | RepeatedByteFlag | ControlAddress, 1, &ControlRegister, 1, 10) != HAL_OK) {
		return false;
	}

	uint8_t EnableRegister = 0x03; // rgbc enable and power on
	if (HAL_I2C_Mem_Write(hi2c, DeviceAddress << 1, CommandFlag | RepeatedByteFlag | EnableAddress, 1, &EnableRegister, 1, 10) != HAL_OK) {
		return false;
	}

	return true;
}

/**
 * @brier Read from color sensor in interrupt mode.
 *
 * @param hi2c[in] - Pointer to I2C handler.
 *
 * @param color_data[out] - Encoded values of red, green, blue and clear components.
 *
 * @return If success.
 */
bool color_sensor_read(I2C_HandleTypeDef * hi2c, uint8_t color_data[8]){
	if (HAL_I2C_Mem_Read_IT(hi2c, DeviceAddress << 1, CommandFlag | AutoIncrementFlag | CDataLAddress, 1, color_data, 8)
			!= HAL_OK) {
		return false;
	}

	return true;
}

/**
 * @brier Return clear component's value.
 *
 * @param color_data[in] - Data from color sensor.
 *
 * @return ADC value for clear component.
 */
uint16_t color_sensor_return_clear(uint8_t color_data[8]){
	return (uint16_t) (color_data[1] << 8) + (uint16_t) color_data[0];
}

/**
 * @brier Return red component's value.
 *
 * @param color_data[in] - Data from color sensor.
 *
 * @return ADC value for red component.
 */
uint16_t color_sensor_return_red(uint8_t color_data[8]){
	return (uint16_t) (color_data[3] << 8) + (uint16_t) color_data[2];
}

/**
 * @brier Return green component's value.
 *
 * @param color_data[in] - Data from color sensor.
 *
 * @return ADC value for green component.
 */
uint16_t color_sensor_return_green(uint8_t color_data[8]){
	return (uint16_t) (color_data[5] << 8) + (uint16_t) color_data[4];
}

/**
 * @brier Return blue component's value.
 *
 * @param color_data[in] - Data from color sensor.
 *
 * @return ADC value for blue component.
 */
uint16_t color_sensor_return_blue(uint8_t color_data[8]){
	return (uint16_t) (color_data[7] << 8) + (uint16_t) color_data[6];
}

/**
 * @brief Return saturation in %.
 *
 * @param color_data[in] - Data from color sensor.
 *
 * @return Saturation in %.
 */
uint16_t color_sensor_return_saturation(uint8_t color_data[8]){
	uint16_t R, G, B, MIN;

	R = color_sensor_return_red(color_data);
	G = color_sensor_return_green(color_data);
	B = color_sensor_return_blue(color_data);

	if(R==0 && G==0 && B==0){
		return 0;
	}

	MIN = R;
	MIN = MIN <= G ? MIN : G;
	MIN = MIN <= B ? MIN : B;

	return (100*((R+G+B)-3*MIN))/(R+G+B);
}

/**
 * @brier Compute hue.
 *
 * @param color_data[in] - Data from color sensor.
 *
 * @return Hue value in degrees (0-360).
 */
uint16_t color_sensor_return_hue(uint8_t color_data[8]) {
	// during computations retval can sometimes exceed uint16_t range
	uint32_t retval;
	uint16_t R, G, B;

	R = color_sensor_return_red(color_data);
	G = color_sensor_return_green(color_data);
	B = color_sensor_return_blue(color_data);

	if (R >= G && G >= B) {
		retval = G - B;
		retval *= 60;
		if(R != B){
			retval /= R - B;
		}
	} else if (G > R && R >= B) {
		retval = 2 * G - R - B;
		retval *= 60;
		retval /= G - B;
	} else if (G >= B && B > R) {
		retval = (2 * G + B) - 3 * R;
		retval *= 60;
		retval /= G - R;
	} else if (B > G && G > R) {
		retval = 4 * B - G - 3 * R;
		retval *= 60;
		retval /= B - R;
	} else if (B > R && R >= G) {
		retval = (4 * B + R) - 5 * G;
		retval *= 60;
		retval /= B - G;
	} else { //(R >= B && B > G)
		retval = 6 * R - 5 * G - B;
		retval *= 60;
		retval /= R - G;
	}

	return retval;
}
