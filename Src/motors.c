/**
 * @file motors.h
 *
 * @date 16.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Library for motors.
 */

#include "motors.h"
#include "mxconstants.h"

/**
 * @brief Safety limit for PWM value.
 */
volatile static uint32_t _pwm_limit;

/**
 * @brief Pointer to timer handle used by H-bridge.
 */
static TIM_HandleTypeDef * _htim_bridge;

/**
 * @brief Channel used by H-bridge to control left motor.
 */
volatile static uint32_t _bridge_left_channel;

/**
 * @brief Channel used by H-bridge to control right motor.
 */
volatile static uint32_t _bridge_right_channel;

/**
 * @brief Initialize motors to work.
 *
 * @param htim_bridge - Pointer to timer handle used by H-bridge.
 * @param bridge_left_channel - Channel used by to generate PWM for left motor.
 * @param bridge_right_channel - Channel used by to generate PWM for right motor.
 * @param pwm_limit - Safety limit for PWM value.
 *
 * @return If everything was successfully updated.
 */
bool motors_init(TIM_HandleTypeDef * htim_bridge, uint32_t bridge_left_channel,
		uint32_t bridge_right_channel, uint32_t pwm_limit){
	bool retval = true;

	_htim_bridge = htim_bridge;
	_bridge_left_channel = bridge_left_channel;
	_bridge_right_channel = bridge_right_channel;
	_pwm_limit = pwm_limit;

	retval = HAL_TIM_PWM_Start(_htim_bridge, _bridge_right_channel) == HAL_OK ? retval : false;
	retval = HAL_TIM_PWM_Start(_htim_bridge, _bridge_left_channel) == HAL_OK ? retval : false;

	return retval;
}

/**
 * @brief Set PWM for motors.
 *
 * @param left - PWM value in % for left motor.
 * @param right - PWM value in % for right motor.
 * @param pwm_limit - Safety limit for PWM value.
 */
void motors_pwm(int32_t left, int32_t right){
	GPIO_PinState State;

	State = (left < 0) ? GPIO_PIN_SET : GPIO_PIN_RESET;
	HAL_GPIO_WritePin(LIN1_GPIO_Port, LIN1_Pin, State);
	HAL_GPIO_WritePin(LIN2_GPIO_Port, LIN2_Pin, !State);

	State = (right < 0) ? GPIO_PIN_SET : GPIO_PIN_RESET;
	HAL_GPIO_WritePin(RIN1_GPIO_Port, RIN1_Pin, !State);
	HAL_GPIO_WritePin(RIN2_GPIO_Port, RIN2_Pin, State);

	if(left < 0){
		left *= -1;
	}

	if(right < 0){
		right *= -1;
	}

	left = (left > _pwm_limit) ? _pwm_limit : left;
	right = (right > _pwm_limit) ? _pwm_limit : right;


	switch(_bridge_left_channel){
	case TIM_CHANNEL_1: _htim_bridge->Instance->CCR1 = left; break;
	case TIM_CHANNEL_2: _htim_bridge->Instance->CCR2 = left; break;
	case TIM_CHANNEL_3: _htim_bridge->Instance->CCR3 = left; break;
	case TIM_CHANNEL_4: _htim_bridge->Instance->CCR4 = left; break;
	}

	switch(_bridge_right_channel){
	case TIM_CHANNEL_1: _htim_bridge->Instance->CCR1 = right; break;
	case TIM_CHANNEL_2: _htim_bridge->Instance->CCR2 = right; break;
	case TIM_CHANNEL_3: _htim_bridge->Instance->CCR3 = right; break;
	case TIM_CHANNEL_4: _htim_bridge->Instance->CCR4 = right; break;
	}
}
