/**
 * @file floor_sensor.c
 * @date 24.02.2016
 * @author Jacek Jankowski
 * @brief Module for floor color sensor.
 */

#include "floor_sensor.h"
#include "rgb_sensor.h"
#include <stdint.h>

/**
 * @brief Pointer to I2C handle used by floor color sensor.
 */
static I2C_HandleTypeDef * _hi2c;

/**
 * @brief Raw data from the RGB sensor.
 */
static uint8_t _data[8];

/**
 * @brief Current floor color.
 */
static enum color_t _color;

/**
 * @brief Initialize module for floor color sensor;
 *
 * @param hi2c - Pointer to I2C handle used by floor color sensor.
 *
 * @return If everything was successfully initialized.
 */
bool floor_sensor_init(I2C_HandleTypeDef * hi2c){
	_hi2c = hi2c;
	return color_sensor_init(_hi2c);
}

/**
 * @brief Update information from RGB sensor.
 *
 * This function should be invoke every 1 ms.
 *
 * @return If everything was successfully updated.
 */
bool floor_sensor_update(void){
	volatile uint16_t saturation = color_sensor_return_saturation(_data);
	volatile uint16_t hue = color_sensor_return_hue(_data);
	if(saturation < 15){ // white
		_color = white_color;
	}
	else if(hue >= 120 && hue <= 300){ // blue
		_color = blue_color;
	}
	else{ // red
		_color = red_color;
	}
	// this is good order because color_sensor_read uses interrupts to deal with i2c
	return color_sensor_read(_hi2c, _data);
}

/**
 * @brief Return current floor color.
 *
 * @return Floor color.
 */
enum color_t floor_sensor_return_color(void){
	return _color;
}
