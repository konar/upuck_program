/**
 * @file localization.c
 * @date 20.02.2016
 * @author Jacek Jankowski
 * @brief Real-time localization module.
 */

#include "localization.h"
#include "map_size.h"

/**
 * @brief Region on the map.
 */
enum region_t {
	red_region, /**< Red base. */
	blue_region, /**< Blue base. */
	white_region, /**< White surface inside the ring. */
	outside_region /**< Space outside the ring. */
};

/**
 * @brief The most probable actual coordinates.
 */
static volatile struct coordinates_t _localization_coordinates;

/**
 * @brief Floor color from the previous update.
 */
static volatile enum color_t _localization_prev_color;

enum region_t _localization_read_map(float x, float y);

/**
 * @brief Return color for given coordinates using map.
 *
 *	This is how the map is represented.
 *
 *
 *	^ Y					OUTSIDE
 *	|
 *	|------------------------------------------------
 *	|									|			|
 *	|									|			|
 *	|									|	RED		|
 *	|									|			|
 *	|									|			|
 *	|									|			|
 *	|									|-----------|
 *	|												| 	OUTSIDE
 *	|												|
 *	|					WHITE						|
 *	|												|
 *	|------------									|
 *	|			|									|
 *	|			|									|
 *	|			|									|
 *	|	BLUE	|									|
 *	|			|									|
 *	|(0,0)		|									|	X
 *	---------------------------------------------------->
 *
 *
 *
 * @param x - x coordinate in m.
 * @param y - y coordinate in m.
 *
 * @return Region for the given coordinates.
 */
enum region_t _localization_read_map(float x, float y){
	if(x>=0 && x<=BASE_X_SIZE && y>=0 && y<=BASE_Y_SIZE){
		return blue_region;
	}
	else if(x>=(RING_X_SIZE-BASE_X_SIZE) && x<=RING_X_SIZE && y>=(RING_Y_SIZE-BASE_Y_SIZE) && y<=RING_Y_SIZE){
		return red_region;
	}
	else if(x<=RING_X_SIZE && x>=0 && y<=RING_Y_SIZE && y>=0){
		return white_region;
	}
	else{
		return outside_region;
	}
}

/**
 * @brief Pose correction.
 *
 * Correct x and y coordinates using information from color detector and odometry module.
 * Corrected coordinates x and y are stored in ::_localization_coordinates.
 *
 * @param[in] x - Value of x coordinated based on odometry.
 * @param[in] y - Value of y coordinated based on odometry.
 * @param[in] color - Detected color.
 */
void _localization_correct_pose(float x, float y, enum color_t color){

	// eliminate outside_region
	x = x<0 ? 0 : x;
	x = x>RING_X_SIZE ? RING_X_SIZE : x;
	y = y<0 ? 0 : y;
	y = y>RING_Y_SIZE ? RING_Y_SIZE : y;


	if(color == blue_color){
		x = x>BASE_X_SIZE ? BASE_X_SIZE : x;
		y = y>BASE_Y_SIZE ? BASE_Y_SIZE : y;
	}
	else if(color == red_color){
		x = x<(RING_X_SIZE-BASE_X_SIZE) ? (RING_X_SIZE-BASE_X_SIZE) : x;
		y = y<(RING_Y_SIZE-BASE_Y_SIZE) ? (RING_Y_SIZE-BASE_Y_SIZE) : y;
	}
	else if(color == white_color){ // color == white
		enum region_t region = _localization_read_map(x,y);
		if(region == blue_region){
			float x_diff = BASE_X_SIZE - x;
			float y_diff = BASE_Y_SIZE - y;
			if(x_diff < y_diff){
				x = BASE_X_SIZE;
			}
			else{
				y = BASE_Y_SIZE;
			}
		}
		else if(region == red_region){
			float x_diff = x - (RING_X_SIZE-BASE_X_SIZE);
			float y_diff = y - (RING_Y_SIZE-BASE_Y_SIZE);
			if(x_diff < y_diff){
				x = RING_X_SIZE-BASE_X_SIZE;
			}
			else{
				y = RING_Y_SIZE-BASE_Y_SIZE;
			}
		}
	}

	_localization_coordinates.x = x;
	_localization_coordinates.y = y;
}

/**
 * @brief Orientation angle correction.
 *
 * Correct orientation angle using information from color detector and odometry module.
 * Corrected coordinates x and y are stored in ::_localization_coordinates.
 *
 * @param[in] theta - Value of orientation angle on odometry.
 * @param[in] color - Detected color for the floor.
 */
void _localization_correct_orientation(float theta, enum color_t color){
	if((_localization_prev_color==blue_color && color==white_color) || (_localization_prev_color==white_color && color==red_color)){
		if(theta > M_PI && theta < 3*M_PI/2){
			theta = (3*M_PI/2-theta) > (theta-M_PI) ? M_PI : 3*M_PI/2;
		}
	}
	else if((_localization_prev_color==white_color && color==blue_color) || (_localization_prev_color==red_color && color==white_color)){
		if(theta > 0 && theta < M_PI/2){
			theta = (M_PI/2-theta) > (theta-0) ? 0 : M_PI/2;
		}
	}

	_localization_prev_color = color;
	_localization_coordinates.theta = theta;
}

/**
 * @brief Real time localization update.
 *
 * @param coordinates_odo - Coordinates computed by odometry module..
 * @param floor_color - Floor color.
 *
 * @return If everything was successfully updated.
 */
bool localization_update(struct coordinates_t coordinates_odo, enum color_t floor_color){
	_localization_correct_pose(coordinates_odo.x, coordinates_odo.y, floor_color);
	_localization_correct_orientation(coordinates_odo.theta, floor_color);
	return true;
}

/**
 * @brief Initialize localization module.
 *
 * @param color_0 - Floor color.
 *
 * @return If everything was successfully initialized.
 */
bool localization_init(enum color_t color_0){
	bool retval = true;
	_localization_prev_color = color_0;

	if(color_0 == blue_color){
		_localization_coordinates.x = BASE_X_SIZE/2;
		_localization_coordinates.y = BASE_Y_SIZE/2;
		_localization_coordinates.theta = M_PI/2;
	}
	else if(color_0 == red_color){
		_localization_coordinates.x = RING_X_SIZE-BASE_X_SIZE/2;
		_localization_coordinates.y = RING_Y_SIZE-BASE_Y_SIZE/2;
		_localization_coordinates.theta = (3*M_PI)/2;
	}
	else{
		retval = false;
	}

	return retval;
}

/**
 * @brief Return the most probable coordinates.
 */
struct coordinates_t localization_return_coordinates(void){
	return _localization_coordinates;
}
