/**
 * @file odometry.h
 *
 * @date 16.02.2016
 *
 * @author Jacek Jankowski
 *
 * @brief Odometry library.
 */

#include "odometry.h"
#include "math.h"

/** Pi value. */
#define M_PI 3.14159265358979323846

/**
 * @brief Actual coordinates.
 */
volatile static struct coordinates_t _odometry_coordinates;

/**
 * @brief Initialize odometry module.
 *
 * Initialize odometry value.
 * Also it can be used to correct odometry,
 * for example in Markov localization or Extended Kalman Filter localization.
 *
 * @param coordinates - Initial values for coordinates.
 *
 * @return If everything was successfully updated.
 */
bool odometry_init(struct coordinates_t coordinates){
	_odometry_coordinates = coordinates;
	return true;
}

/**
 * @brief Real time odometry computation.
 *
 * Real time odometry computation.
 * This function should be invoked every 1 ms.
 *
 * @param v - Actual forward velocity in m/s.
 * @param omega - Actual angular velocity in rad/s.
 *
 * @return If everything was successfully updated.
 */
bool odometry_update(float v, float omega){
	float delta_theta = omega * 0.001; // 0.001 because this function shoudl be invoked every 1ms

	_odometry_coordinates.x += v * cosf(_odometry_coordinates.theta + delta_theta/2) * 0.001;
	_odometry_coordinates.y += v * sinf(_odometry_coordinates.theta + delta_theta/2) * 0.001;
	_odometry_coordinates.theta += delta_theta;
	while (_odometry_coordinates.theta > (2*M_PI)){
		_odometry_coordinates.theta -= 2*M_PI;
	}
	while(_odometry_coordinates.theta < 0){
		_odometry_coordinates.theta += 2*M_PI;
	}

	return true;
}

/**
 * @brief Return coordinates in metric units.
 *
 * Return computed coordinates in metric units:
 * x pose, y pose and orientation angle (theta).
 *
 * @return Coordinates in metric units.
 */
struct coordinates_t odometry_return_coordinates(void){
	return _odometry_coordinates;
}
